// app.js

/**
 * Required External Modules
 */

const express = require("express")
const path = require("path")
var serveIndex = require('serve-index');

/**
 * App Variables
 */

const app = express()
const port = process.env.PORT || "8000";

const stylesheetPath = 'public/css/dirlist.css';

/**
 *  App Configuration
 */

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.set(express.static(path.join(__dirname, "public")));

/**
 * Routes Definitions
 */

 app.get("/", (req, res) => {
    res.render("index", { title: "Different Cast" });
  });

  /**
 * Serve Files
 */

app.use('/records', express.static('records'), 
serveIndex('records', {
  'icons': true,
  stylesheet: stylesheetPath,
}))


/**
 * Server Activation
 */

app.listen(port, function() {
    console.log(`Listening to requests on http://localhost:${port}`)
});